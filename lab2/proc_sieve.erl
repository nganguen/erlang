-module(proc_sieve).

-export([generate/1]).
-export([gen_print/1]).
-export([sieve/0]).

gen_print(MaxN) ->
	lists:foreach(fun(X)->io:format("~w ",[X]) end, generate(MaxN)).

generate(MaxN) when MaxN > 1 ->
	PID = spawn(proc_sieve, sieve, []),
	generate_help(2, MaxN, PID);
generate(_) ->
	[].

generate_help(CurNum, MaxN, PID) when CurNum =< MaxN ->
	PID ! CurNum,
	generate_help(CurNum + 1, MaxN, PID);
generate_help(_, _, PID) ->
	PID ! {done, self()},
	receive
		{result, List} ->
			List;
		_ ->
			{error, "Wrong output"}
	end.

sieve() ->
	receive
		FilterBy ->
			sieve_help(FilterBy, nil, nil)
	end.

sieve_help(Filter, RedirectPid, SavedBackPid) ->
	receive
		{done, BackPID} ->
			if
				RedirectPid == nil ->
					BackPID ! {result, [Filter]};
				true ->
					RedirectPid ! {done, self()},
					sieve_help(Filter, nil, BackPID)
			end;
		{result, List} ->
			if
				SavedBackPid =/= nil ->
						SavedBackPid ! {result, [Filter | List]};
				true ->
					nil
			end;
		CurNum when (CurNum rem Filter) =/= 0 ->
			if
				RedirectPid == nil ->
					PID = spawn(proc_sieve, sieve, []),
					PID ! CurNum,
					sieve_help(Filter, PID, SavedBackPid);
				true ->
					RedirectPid ! CurNum,
					sieve_help(Filter, RedirectPid, SavedBackPid)
			end;
		_ ->
			sieve_help(Filter, RedirectPid, SavedBackPid)
	end.
