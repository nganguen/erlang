-module(mobius).

-export([is_prime/1]).
-export([prime_factors/1]).
-export([is_square_multiple/1]).
-export([find_square_multiples/2]).

is_prime(N) ->
	is_prime_help(2, N).

is_prime_help(Divisor, N) when (Divisor * Divisor) =< N ->
	if
		(N rem Divisor) == 0 ->
			false;
		true ->
			is_prime_help(Divisor + 1, N)
	end;

is_prime_help(_, _) -> true.

prime_factors(0) ->
	[];

prime_factors(N) ->
	prime_factors_help(N, 2, [1]).

prime_factors_help(N, Factor, CurResult) when Factor =< N ->
	if
		(N rem Factor) == 0 ->
			IsPrime = is_prime(Factor),
			if 
				IsPrime == true ->
					prime_factors_help(N, Factor + 1, [Factor|CurResult]);
				true ->
					prime_factors_help(N, Factor + 1, CurResult)
			end;
		true ->
			prime_factors_help(N, Factor + 1, CurResult)
	end;

prime_factors_help(_, _, Result) ->
	Result.

is_square_multiple(N) when N < 4 ->
	false;

is_square_multiple(N) ->
	is_square_multiple_help(N, 2).

is_square_multiple_help(N, Factor) when Factor =< N ->
	if
		(N rem Factor) == 0 ->
			IsPrime = is_prime(Factor),
			if 
				IsPrime == true ->
					if
						(N rem (Factor * Factor)) == 0 ->
							true;
						true ->
							is_square_multiple_help(N, Factor + 1)
					end;
				true ->
					is_square_multiple_help(N, Factor + 1)
			end;
		true ->
			is_square_multiple_help(N, Factor + 1)
	end;

is_square_multiple_help(_, _) ->
	false.

find_square_multiples(Count, MaxN) ->
	find_square_multiples_help(0, fail, 0, Count, MaxN).

find_square_multiples_help(CurNum, First, FoundNum, NeedFind, Limit) 
when CurNum =< Limit , FoundNum < NeedFind ->
	IsSquare = is_square_multiple(CurNum),
	if
		IsSquare == true ->
			if
				First == fail ->
					find_square_multiples_help(CurNum + 1, CurNum, FoundNum + 1, NeedFind, Limit);
				true ->
					find_square_multiples_help(CurNum + 1, First, FoundNum + 1, NeedFind, Limit)
			end;
		true ->
			find_square_multiples_help(CurNum + 1, fail, 0, NeedFind, Limit)
	end;

find_square_multiples_help(_, First, FoundNum, NeedFind, _) when FoundNum == NeedFind ->
	First;

find_square_multiples_help(_, _, _, _, _) ->
	fail.

