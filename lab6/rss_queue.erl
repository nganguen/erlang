-module(rss_queue).
-include("logging.hrl").
-define(TIMEOUT,10000).
-include_lib("xmerl/include/xmerl.hrl").

-behaviour(gen_server).
-export([handle_call/3, handle_cast/2, handle_info/2, terminate/2,
         code_change/3]).
-export([init/1, start/1, start/2, add_item/2, add_feed/2, 
          get_all/1, broadcast/2, add_item_to_q/4, subscribe/2]).

-record(rssQ,{queue,subscribers}).

start(Name) -> 
  gen_server:start({local, Name}, ?MODULE, [], []).

start(Name,Url)->
  gen_server:start({local, Name}, ?MODULE, [Url], []).

% @doc Запускает очередь в "автономном" режиме
init([]) ->
  process_flag(trap_exit,true),
  {ok, #rssQ{queue=[],subscribers=sets:new()} };

% @doc Получает URL, и запускает очередь в режиме "чтения"
init([Url]) -> 
  State = #rssQ{queue=[],subscribers=sets:new()},
  process_flag(trap_exit,true),
  rss_reader:start(Url,self()),
  {ok, State }.

% @doc Функция вызывается, когда gen_server получает запрос с функцией gen_server:call/2
% Подписывает указанную очередь RSS на получение сообщений очереди. 
handle_call(_Request={subscribe,QPid}, _From, State=#rssQ{queue=Q,subscribers=Subs}) ->
  {Reply,NewState} = case sets:is_element(QPid,Subs) of
    true -> {{error,already_subscribed},State};
    false ->  erlang:monitor(process,QPid),
    ?INFO("New subscriber ~p to ~p~n",[QPid,self()]),
    [add_item(QPid,Item) || Item <- Q],
    {ok,State#rssQ{subscribers=sets:add_element(QPid,Subs)}}
  end,
  {reply,Reply, NewState};

% Запрашивает текущий список элементов RSS ленты
handle_call(_Request={get_all}, _From, State=#rssQ{queue=Q}) -> 
  {reply,Q,State};
handle_call(_Request, _From, State) -> 
  {reply,{error,{unknown_request,_Request}}, State}.

% @doc Вызывается когда gen_server получает запрос, посланый с использованием 
% функции gen_server:cast/2
% Добавляет элемент во внутреннюю очередь, если он новый или обновленный
handle_cast(_Msg={add_item,RSSItem=#xmlElement{name=item}}, State=#rssQ{queue=Q,subscribers=Subs}) -> 
  NewQ = add_item_to_q(RSSItem,Q,Subs),
  {noreply,State#rssQ{queue=NewQ}};

% Удаляет указанную очередь RSS из списка подписчиков.
handle_cast(_Msg={unsubscribe,QPid}, State=#rssQ{subscribers=Subs}) -> 
  {noreply,State#rssQ{subscribers=sets:del_element(QPid,Subs)}};

handle_cast(_Msg, State) -> 
  ?WARN("Unknown msg {~p} to Q{~p}",[_Msg,State]),
  {noreply, State}.

% @doc Эта функция вызывается, когда происходит таймаут ожидания 
% сообщений или когда приходит любое сообщение, 
% отличное от асинхронного, синхронного запросов.
handle_info(_Info={'DOWN',_,_,QPid,_Reason},State=#rssQ{subscribers=Subs})->
  {noreply, State#rssQ{subscribers=sets:del_element(QPid,Subs)}};

handle_info(_Info={'EXIT',FromPid,_Reason},State)->
  ?ERROR("RSS Reader ~p died for ~p with reason ~n",[FromPid,self(),_Reason]),
  {noreply, State};

handle_info(_Info, State) -> 
  {noreply, State}.

% @doc Эта функция вызывается перед завершением gen_server процесса 
% Как только происходит возврат контроля в gen_server процесс, он завершается с причиной 
% Reason. Возвращаемое значение этой функции игнорируется.
% @spec terminate(Reason, State) -> void()
terminate(_Reason, _State) -> ok.

% @doc Эта функция вызывается, когда происходит изменение кода
% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
code_change(_OldVsn, State, _Extra) -> {ok, State}.

% @doc Эти функции подписывают очередь Queue1 на сообщения очереди Queue2.
subscribe(From,To)->
  gen_server:call(To,{subscribe,From}).

%% @doc Добавляет новый элемент в очередь 
add_item_to_q(NewItem,Q,Subs)->
  add_item_to_q(NewItem,[],Q,Subs).

add_item_to_q(NewItem,L1,[],Subs)->
  ?INFO("New item ~p ~n",[self()]),
  broadcast(NewItem,Subs),
  L1++[NewItem];

add_item_to_q(NewItem,L1,L=[OldItem|Rest],Subs)->
  case rss_parse:compare_feed_items(OldItem,NewItem) of
    same -> 
      L1++L;
    updated -> 
      ?INFO("Updated item ~p ~n",[self()]),
      broadcast(NewItem,Subs),
      L1++Rest++[NewItem] ;
    different -> 
      add_item_to_q(NewItem,L1++[OldItem],Rest,Subs)
  end.

% @doc Упрощает процедуру отправки элемента в очередь
add_item(QPid,Item)->
  ok = gen_server:cast(QPid , {add_item,Item} ),ok.

% @doc Извлекает все элементы из документа ленты
% и отправляет все элементы по порядку в очередь
add_feed(QPid,RSS2Feed) when is_pid(QPid) ->
  Items = rss_parse:get_feed_items(RSS2Feed),
  [add_item(QPid,Item) || Item <- Items],
  ?INFO("Added N=~p items from the feed to ~p ~n",[length(Items),QPid]), 
  ok.

%@doc Упрощает процедуру получения списка элементов ленты от процесса
get_all(QPid)->
  gen_server:call(QPid,{get_all}).

% @doc Передает объект нескольким PID
broadcast(Item,PidSet)->
  [add_item(Pid,Item) || Pid <- sets:to_list(PidSet)]. 