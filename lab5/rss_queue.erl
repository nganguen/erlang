-module(rss_queue).
-include("logging.hrl").
-export([init/1, start/0, start/1, server/2, add_item/2, add_feed/2, 
          get_all/1, broadcast/2, add_item_to_q/4]).
-define(TIMEOUT,10000).

init([])->
  start();
init([Url])->
  start(Url).

% @doc Запускает новый процесс и возвращает PID созданного процесса
start()->
  Q = [],
  spawn(?MODULE,server,[Q,sets:new()]).

start(Url)->
  QPid = start(),
  rss_reader:start(Url,QPid),
  QPid.

% @doc Цикл сервера процесса очереди RSS   
server(Q,Subs)->
  receive

% @doc Регистрация новых элементов в очереди
    {add_item,RSSItem} ->
      NewQ = add_item_to_q(RSSItem,Q,Subs), 
      server(NewQ,Subs);

% @doc Другой процесс может отправить это сообщение очереди RSS,
% чтобы получить все содержимое очереди.
    {get_all,ReqPid} ->
      ReqPid ! {self(),Q},
      ?INFO("Sent rss items to ~p~n",[ReqPid]),
      server(Q,Subs);

% @doc Удаляет заданный PID из множества подписчиков
    {unsubscribe,QPid} ->
      server(Q,sets:del_element(QPid,Subs));

% @doc Позволяет очереди подписаться на получение элементов ленты другой очереди
    {subscribe,QPid} ->
      erlang:monitor(process,QPid),
      ?INFO("New subscriber ~p to ~p~n",[QPid,self()]),
      [add_item(QPid,Item) || Item <- Q],
      server(Q,sets:add_element(QPid,Subs));
% @doc Удаляет PID очереди из списка подписчиков      
    {'DOWN',_,_,QPid,_Reason}->
      server(Q,sets:del_element(QPid,Subs));
    _Msg -> io:format("Unknown msg~p~n",[_Msg])  
  end.

% @doc Упрощает процедуру отправки элемента в очередь
add_item(QPid,Item)->
  QPid ! {add_item,Item},
  ok.

% @doc Извлекает все элементы из документа ленты
% и отправляет все элементы по порядку в очередь
add_feed(QPid,RSS2Feed) when is_pid(QPid) ->
  Items = rss_parse:get_feed_items(RSS2Feed),
  [add_item(QPid,Item) || Item <- Items],
  ?INFO("Added N=~p items from the feed to ~p ~n",[length(Items),QPid]), 
  ok.
% @doc Упрощает процедуру получения списка элементов ленты от процесса
get_all(QPid) when is_pid(QPid)->
  QPid!{get_all,self()},
  receive
    {QPid,Q} -> Q ;
    _Msg -> {error,unknown_msg,_Msg}
  after 
    ?TIMEOUT -> {error,timeout}
  end.

% @doc Передает объект нескольким PID
broadcast(Item,PidSet)->
  [add_item(Pid,Item) || Pid <- sets:to_list(PidSet)].

% @doc Добавляет новый элемент в очередь
add_item_to_q(NewItem,L1,[],Subs)->
  ?INFO("New item ~p ~n",[self()]),
  broadcast(NewItem,Subs), L1++[NewItem];

% @doc Обновляет элемент
add_item_to_q(NewItem,L1,L=[OldItem|Rest],Subs)->
  case rss_parse:compare_feed_items(OldItem,NewItem) of
    same -> 
      L1++L;
    updated -> 
      ?INFO("Updated item ~p ~n",[self()]),
      broadcast(NewItem,Subs),
      L1++Rest++[NewItem] ;
    different -> 
      add_item_to_q(NewItem,L1++[OldItem],Rest,Subs)
  end.
  
add_item_to_q(NewItem,Q,Subs)->
  add_item_to_q(NewItem,[],Q,Subs).